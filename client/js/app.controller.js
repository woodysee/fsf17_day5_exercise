/**
 * Client_side app.controller.js
 * Angular js
 */
"use strict";

(
  () => {
    "use strict"
    angular.module("PopQuizApp").controller("PopQuizViewModel", PopQuizViewModel);

    PopQuizViewModel.$inject = ["$http"];

    function PopQuizViewModel($http) {
      //Initialise all viewmodel variables
      const vm = this;
      vm.quiz = {};
      vm.shuffled = null; //initialises the order of the quiz by /:id
      vm.submission = {
        choice: [],
        score: 0
      };
      vm.current = null; //indicates the current question number by shuffleArray index
      vm.next_btn_state_disabled = true;

      //Initialise all viewmodel methods
      vm.submitQn = submitQn;
      vm.submitQuiz = submitQuiz;

      //Initialise quiz
      (() => {
        //The Fischer-Yates shuffle on an array
        const shuffle = (arr) => {
          let i, j = 0;
          let dat = null;
          for (i = arr.length - 1; i > 0; i--) {
            j = Math.floor(
              Math.random() * (i + 1)
            );
            dat = arr[i];
            arr[i] = arr[j];
            arr[j] = dat;
          };
          return arr;
        };
        
        $http.get("/api/popquizzes").then(
          (quizzes) => {
            vm.order = quizzes.data;
            vm.shuffled = shuffle(quizzes.data);
            console.log(vm.shuffled);
            vm.current = 0;
            console.log(`vm.current is ${vm.current}`);
            // Submit the scrambled array into a new object
          }
        ).catch(
          (err) => {
            console.log(err);
          }
        );
      })();
      
      function submitQn() {
        vm.nextBtnDisabled = false;
        $http.post(
          "/api/popquizzes/submit/",
          {
            'id': vm.shuffled[vm.current].id,
            'answer': vm.submission.choice[vm.current]
          }
        ).then(
          (result) => {
            console.log(result);
          }
        ).catch(
          (err) => {
            console.log(err);
          }
        );
      };

      //On quizzes completion
      submitQuiz() {
        $http.post(
          "/api/submitquizzes",
          vm.finalAnswer
        ).then(
          (result) => {
            vm.isCorrect = result.data;
            console.log(result.data);
          }
        ).catch(
          (e) => {
            console.log(e);
          }
        );
      };
      
    };
  }
)();
