/**
*App.js server side
*/

//Dependencies
const express = require("express");
const app = express(); 
const bodyParser = require("body-parser");

const NODE_PORT = process.env.PORT || 4000;

app.use(express.static(__dirname + "/../client/"));
app.use(bodyParser.urlencoded({limit:'50mb', extended: true}));
app.use(bodyParser.json({limit:'50mb'}));

let quizzes = require("./quizzes.json");
let shuffledQuizzes = [];

// console.log(quizzes);

//Methods

/* GET all quizzes */
app.get(
  "/api/popquizzes",
  (req, res) => {
    console.log("200: GET /api/popquizzes");
    res.status(200).json(quizzes);
  }
);

/* GET all quizzes shuffled */
app.get(
  "/api/shuffled",
  (req, res) => {
    console.log("200: GET /api/shuffled");
    res.status(200).json(shuffledQuizzes);
  }
);

/* GET 1 by :id */
app.get(
    "/api/popquizzes/:id",
    (req, res) => {
      const id = req.params.id;
      let quiz = quizzes.filter(
        (quizzes) => {
          return quizzes.id == id;
        }
      );
      console.log("200: GET /api/popquizzes/" + id);
      res.status(200).json(quiz);
    }
);

/* SUBMIT 1 by :id */
app.post(
  "/api/popquizzes/submit",
  (req, res) => {
    const ans = req.body;
    console.log(ans);
    console.log("200: POST /api/popquizzes/" + id);
    res.status(200).json(quiz);
  }
);

app.listen(
    NODE_PORT,
    () => {
        console.log(`Listening with server/app.js at port ${NODE_PORT}`);
    }
);
