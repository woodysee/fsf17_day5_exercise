A Angular app by Javier, Davis & Woody

## Deliverables
* 5 pop quizzes
* Sequence of the questions must be randomised
* Comment box
* Submit button will be disabled when it is lit up

If correct, it will display correct, unactivate the button
If wrong, it will display wrong + continue.

At the end of the quiz, `end quiz` button next to the next to the submit button. Submit button and end quiz button is 

## Tips
`ng-show` & `ng-hide` (conditions and booleans can be shown in the directives)

## Steps
1. `git clone` <url>
2. We are now working on the app now.
3. `npm init`
4. Install dependencies:
  - `npm install express --save`
  - `npm install body-parser --save`
5. Create `/client` for front-end and `/server` for back-end apps.
6. Create `.bowerrc`
7. Add in `.bowerrc`:

`
{
    "directory": "client/bower_components"
}
`

8. Before pushing, remembering to add `bower_components` and `node_modules` to your `.gitignore` file.

9.



## Instructions
1. Create the routes first, e.g. `GET`, `POST` in `server/app.js`.

## Front-end
1. You must have an `ng-app` at the html level in ==> `ng-app="PopQuizApp"`

## Deployment to Heroku
heroku login

# At project level...
- `heroku create`
  - to check if added to remote
- `git remote -v`
  - to add if not existing
- `git remote add heroku https://git.heroku.com/<something>.git`    - <something> should be generated after `heroku create`
- `git push heroku master`